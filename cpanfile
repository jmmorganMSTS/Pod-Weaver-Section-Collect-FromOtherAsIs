requires 'perl', '5.010';

requires 'Pod::Weaver::Section::Collect::FromOther';

on 'develop' => sub {
    requires 'Dist::Milla', '1.0.17';
    requires 'Dist::Zilla::Plugin::MetaResourcesFromGit';
    requires 'Dist::Zilla::Plugin::Pinto::Add';
    requires 'Dist::Zilla::Plugin::PodWeaver';
};

on test => sub {
    requires 'Test::More', '0.96';
};
