# NAME

Pod::Weaver::Section::Collect::FromOtherAsIs - Import sections from other POD, without transformations

# VERSION

version 0.01

# SYNOPSIS

    use Pod::Weaver::Section::Collect::FromOtherAsIs;

# DESCRIPTION

Pod::Weaver::Section::Collect::FromOtherAsIs is a subclass of
Pod::Weaver::Section::Collect::FromOther that defaults to not applying any
transformations to the POD included from other modules.

# SEE ALSO

[Pod::Weaver::Section::Collect::FromOther](https://metacpan.org/pod/Pod::Weaver::Section::Collect::FromOther)

# AUTHOR

James Morgan <jmmorgan@multiservice.com>

# COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by Multi Service Technology Solutions.  No
license is granted to other entities.
