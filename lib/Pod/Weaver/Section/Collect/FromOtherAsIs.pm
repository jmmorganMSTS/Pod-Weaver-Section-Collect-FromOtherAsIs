package Pod::Weaver::Section::Collect::FromOtherAsIs;
# ABSTRACT: Import sections from other POD, without transformations

use 5.010;
our $VERSION = '0.01';

use Moose;
use namespace::autoclean;

use Moose::Autobox;

use Path::Class;

use Pod::Elemental::Selectors -all;
use Pod::Elemental;
use Pod::Elemental::Transformer::List::Converter;
use Pod::Elemental::Transformer::Nester;

# debugging...
#use Smart::Comments '###';

extends 'Pod::Weaver::Section::Collect::FromOther';

has '+command' => (
    default => 'from_other_as_is',
);

sub copy_sections_from_other {
    my ($self, $module, $header_text, $command) = @_;

    ### find our remote nodes to copy...
    my $selector = s_command('head1');
    my $fn = $self->_find_module($module);

    ### $fn
    my $other_doc = Pod::Elemental->read_file($fn);
    Pod::Elemental::Transformer::Pod5->new->transform_node($other_doc);

    ### attack \$other_doc!...
    my @newbies;
    $command ||= 'as_is';
    my $as_is         = $command eq 'as_is' ? 1 : 0;
    my $found_command = $command eq 'all' ? 1 : $as_is;

    if ( !$as_is ) {
        my $list_transform = Pod::Elemental::Transformer::List::Converter->new;
        $list_transform->transform_node($other_doc);
    }

    my $nester = Pod::Elemental::Transformer::Nester->new({
         top_selector      =>  s_command('head1'),
         content_selectors => [
             s_command([ qw(head2 head3 head4 over item back) ]),
             s_flat,
         ],
    });
    $nester->transform_node($other_doc);

    $other_doc->children->each_value(sub {

        return unless $_->content eq $header_text;

        my @children = @{ $_->children };

        for my $child (@children) {

            # XXX we likely want to make this optional
            do { $found_command++ } if $child->does('Pod::Elemental::Command');
            next unless $found_command;
            push @newbies, $child;
        }
    });

    ### @newbies
    return (scalar @newbies ? (@newbies) : ());
}

1;

__END__

=pod

=encoding utf-8

=head1 NAME

Pod::Weaver::Section::Collect::FromOtherAsIs - Import sections from other POD, without transformations

=head1 VERSION

version 0.01

=head1 SYNOPSIS

  use Pod::Weaver::Section::Collect::FromOtherAsIs;

=head1 DESCRIPTION

Pod::Weaver::Section::Collect::FromOtherAsIs is a subclass of
Pod::Weaver::Section::Collect::FromOther that defaults to not applying any
transformations to the POD included from other modules.

=head1 SEE ALSO

L<Pod::Weaver::Section::Collect::FromOther|Pod::Weaver::Section::Collect::FromOther>

=head1 AUTHOR

James Morgan <jmmorgan@multiservice.com>

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2017 by Multi Service Technology Solutions.  No
license is granted to other entities.

=cut
